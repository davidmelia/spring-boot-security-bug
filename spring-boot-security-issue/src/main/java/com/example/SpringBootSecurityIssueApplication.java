package com.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootSecurityIssueApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootSecurityIssueApplication.class, args);
	}
}
