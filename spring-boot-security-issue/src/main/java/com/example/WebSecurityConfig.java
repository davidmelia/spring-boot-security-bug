package com.example;

import java.util.Arrays;

import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

@EnableWebSecurity
@Order(SecurityProperties.ACCESS_OVERRIDE_ORDER)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    
   @Override
   protected void configure(AuthenticationManagerBuilder auth) throws Exception {
      auth.authenticationProvider(createDaveProvider());
   }
   
 //@Bean
 public DaveProvider createDaveProvider(){
    return new DaveProvider();
 }
 
 public class DaveProvider implements AuthenticationProvider {

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
       if(!authentication.getName().equals("dave")) {
          throw new UsernameNotFoundException("user must be 'dave' - shhhhhhhh");
       } else {
          return new UsernamePasswordAuthenticationToken("dave", "melia", Arrays.asList(new SimpleGrantedAuthority("SOMEOTHERROLE")));
       }
    }

    @Override
    public boolean supports(Class<?> authentication) {
       // TODO Auto-generated method stub
       return true;
    }
    
 }
   
   protected void configure(HttpSecurity http) throws Exception {
     
      http
         .authorizeRequests().antMatchers("/index.html").permitAll()
            .anyRequest().authenticated()
            .and()
         .formLogin();
   }
}
