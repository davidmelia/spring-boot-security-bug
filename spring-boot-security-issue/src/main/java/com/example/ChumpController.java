package com.example;

import java.util.HashMap;
import java.util.Map;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ChumpController {

   @RequestMapping(value="/someRestCall",produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
   public Map<String,String> someRestCall(){
      Map<String, String> response = new HashMap<>();
      response.put("hello", "my friend");
      return response;
   }
   
}
